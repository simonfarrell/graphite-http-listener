const net = require('net')

const connect_and_send = (host, port, message) => {

    var p = new Promise((resolve, reject) => {
	const client = net.createConnection({ host: host, port: port }, () => {
	    // 'connect' listener.
	    console.log('Connecting to ' + host + ':' + port)
	});
	client.on('ready', () => {
	    console.log('Writing ' + message);
	    client.write(message, 'utf8', () => {
		client.end();
	    });
	});
	client.on('data', (data) => {
	    console.log("Received: " + data.toString());
	    client.end();
	    reject("Oops");
	});
	client.on('end', () => {
//	    console.log('disconnected from server');
	    resolve("ok");
	});
    });
    return p;
}

module.exports = connect_and_send

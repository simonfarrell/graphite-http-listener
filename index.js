const express = require('express')
const carbon = require('./carbon')

const app = express()
const port = 3001

app.get('/status', (req, res) => {
    req.status(200).end()
})

app.get('/', (req, res) => {

    if(typeof req.query.server == 'string'  &&
       typeof req.query.metric == 'string' &&
       parseInt(req.query.value) > 0) {
	// send to graphite
	carbon(req.query.server, req.query.metric, req.query.value)
        .then(()=> {
	    console.log('Sent ' + req.query.server + ',' + req.query.metric + ' = ' + req.query.value)
	    res.status(200).end();
	})
	.catch((err) => {
	    console.log(err)
	    res.status(500).send(err)
	})
    } else {
	res.status(400).send('Bad or missing query parameters: ' + req.query)
    }	    
})

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
	       })

//c('test.simon2', 'node', 33)
